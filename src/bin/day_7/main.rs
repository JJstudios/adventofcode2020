use std::collections::HashMap;

use anyhow::{ensure, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<HashMap<String, Vec<(String, u32)>>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(.+) bags contain (.+)$").unwrap();
        static ref RE_INNER: Regex = Regex::new(r"(\d+) ([^\.,]+) bags?").unwrap();
    }

    let mut bag_rules = HashMap::new();
    for line in input.lines() {
        let caps = RE
            .captures(line)
            .with_context(|| format!("Failed to parse line in input: '{}'", line))?;
        let outer_bag = caps[1].to_string();
        let mut rules = Vec::new();
        for inner_caps in RE_INNER.captures_iter(&caps[2]) {
            let inner_bag = inner_caps[2].to_string();
            let inner_num = inner_caps[1].parse::<u32>().unwrap();
            rules.push((inner_bag, inner_num));
        }
        bag_rules.insert(outer_bag, rules);
    }

    Ok(bag_rules)
}

fn run_part1(bag_rules: &HashMap<String, Vec<(String, u32)>>) -> u32 {
    bag_rules
        .keys()
        .filter(|b| bag_could_contain(bag_rules, b, "shiny gold"))
        .count() as u32
}

fn bag_could_contain(bag_rules: &HashMap<String, Vec<(String, u32)>>, outer_bag: &str, bag_to_find: &str) -> bool {
    let rules = bag_rules.get(outer_bag).unwrap();
    for rule in rules {
        let inner_bag = &rule.0;
        if inner_bag == bag_to_find {
            return true;
        }
        if bag_could_contain(bag_rules, inner_bag, bag_to_find) {
            return true;
        }
    }
    false
}

fn run_part2(bag_rules: &HashMap<String, Vec<(String, u32)>>) -> u32 {
    how_many_bags_in(bag_rules, "shiny gold")
}

fn how_many_bags_in(bag_rules: &HashMap<String, Vec<(String, u32)>>, outer_bag: &str) -> u32 {
    let rules = bag_rules.get(outer_bag).unwrap();
    rules
        .iter()
        .map(|(inner_bag, inner_num)| inner_num * (how_many_bags_in(bag_rules, inner_bag) + 1))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = parse_input(include_str!("input_example1")).unwrap();

        assert_eq!(run_part1(&input), 4);
        assert_eq!(run_part2(&input), 32);
    }

    #[test]
    fn test_example2() {
        let input = parse_input(include_str!("input_example2")).unwrap();

        assert_eq!(run_part2(&input), 126);
    }
}

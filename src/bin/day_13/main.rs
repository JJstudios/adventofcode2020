use anyhow::{ensure, Result};
use num::integer::lcm;

fn main() -> Result<()> {
    let (earliest_departure, buses) = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(earliest_departure, &buses)?);
    println!("Part 2: {:?}", run_part2(&buses));

    Ok(())
}

fn parse_input(input: &str) -> Result<(u32, Vec<Option<u32>>)> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let mut lines = input.lines();
    let earliest_departure = lines.next().unwrap().parse::<u32>()?;
    let buses = lines
        .next()
        .unwrap()
        .split(',')
        .map(|b| b.parse::<u32>().ok())
        .collect();

    Ok((earliest_departure, buses))
}

fn run_part1(earliest_departure: u32, buses: &[Option<u32>]) -> Result<u32> {
    let bus_departures = buses
        .iter()
        .filter_map(|&b| b)
        .map(|b| (b, first_departure_after(b, earliest_departure)));
    let earliest_bus_departure = bus_departures.min_by_key(|&t| t.1).unwrap();

    Ok(earliest_bus_departure.0 * (earliest_bus_departure.1 - earliest_departure))
}

fn first_departure_after(bus: u32, earliest_departure: u32) -> u32 {
    let remainder = earliest_departure % bus;
    if remainder == 0 {
        earliest_departure
    } else {
        earliest_departure - remainder + bus
    }
}

fn run_part2(buses: &[Option<u32>]) -> u64 {
    let mut t = 0;
    let mut increment = 1;
    for (dt, &ob) in buses.iter().enumerate() {
        if let Some(bus) = ob {
            loop {
                if (t + dt as u64) % bus as u64 == 0 {
                    increment = lcm(increment, bus as u64);
                    break;
                }

                t += increment;
            }
        }
    }
    t
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let (earliest_departure, buses) = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(earliest_departure, &buses).unwrap(), 295);
        assert_eq!(run_part2(&buses), 1068781);
    }

    #[test]
    fn test_part2_example1() {
        let buses = vec![Some(17), None, Some(13), Some(19)];
        assert_eq!(run_part2(&buses), 3417);
    }

    #[test]
    fn test_part2_example2() {
        let buses = vec![Some(67), Some(7), Some(59), Some(61)];
        assert_eq!(run_part2(&buses), 754018);
    }

    #[test]
    fn test_part2_example3() {
        let buses = vec![Some(67), None, Some(7), Some(59), Some(61)];
        assert_eq!(run_part2(&buses), 779210);
    }

    #[test]
    fn test_part2_example4() {
        let buses = vec![Some(67), Some(7), None, Some(59), Some(61)];
        assert_eq!(run_part2(&buses), 1261476);
    }

    #[test]
    fn test_part2_example5() {
        let buses = vec![Some(1789), Some(37), Some(47), Some(1889)];
        assert_eq!(run_part2(&buses), 1202161486);
    }
}

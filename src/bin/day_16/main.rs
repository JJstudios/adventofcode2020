use anyhow::{ensure, Context, Error, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::{HashMap, HashSet, VecDeque};
use std::str::FromStr;

struct Rule {
    name: String,
    low1: u32,
    high1: u32,
    low2: u32,
    high2: u32,
}

struct Ticket {
    values: Vec<u32>,
}

impl Rule {
    pub fn matches(&self, value: u32) -> bool {
        (self.low1 <= value && value <= self.high1) || (self.low2 <= value && value <= self.high2)
    }
}

impl FromStr for Rule {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^([a-z ]+): (\d+)-(\d+) or (\d+)-(\d+)$").unwrap();
        }

        let caps = RE
            .captures(s)
            .with_context(|| format!("Failed to parse line in input: '{}'", s))?;

        Ok(Rule {
            name: caps[1].to_string(),
            low1: caps[2].parse()?,
            high1: caps[3].parse()?,
            low2: caps[4].parse()?,
            high2: caps[5].parse()?,
        })
    }
}

impl FromStr for Ticket {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        let values = s
            .trim()
            .split(',')
            .map(|s| s.parse())
            .collect::<Result<Vec<_>, _>>()
            .with_context(|| format!("Failed to parse line in input: '{}'", s))?;
        Ok(Ticket { values })
    }
}

fn main() -> Result<()> {
    let (rules, your_ticket, nearby_tickets) = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&rules, &nearby_tickets));
    println!("Part 2: {:?}", run_part2(&rules, &your_ticket, &nearby_tickets));

    Ok(())
}

fn parse_input(input: &str) -> Result<(Vec<Rule>, Ticket, Vec<Ticket>)> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?s)^(.*)\nyour ticket:\n(.*)\nnearby tickets:\n(.*)$").unwrap();
    }

    let caps = RE.captures(input).context("Failed to parse input")?;

    let rules = caps[1]
        .lines()
        .map(|l| l.parse())
        .collect::<Result<Vec<_>, _>>()
        .context("Failed to parse rules section")?;
    let your_ticket = caps[2].parse().context("Failed to parse your ticket section")?;
    let nearby_tickets = caps[3]
        .lines()
        .map(|l| l.parse())
        .collect::<Result<Vec<_>, _>>()
        .context("Failed to parse nearby tickets section")?;

    Ok((rules, your_ticket, nearby_tickets))
}

fn run_part1(rules: &[Rule], nearby_tickets: &[Ticket]) -> u32 {
    let all_ticket_values: Vec<u32> = nearby_tickets.iter().map(|t| &t.values).flatten().cloned().collect();

    let mut scanning_error_rate = 0;
    for value in all_ticket_values {
        let mut match_found = false;
        for rule in rules {
            if rule.matches(value) {
                match_found = true;
            }
        }
        if !match_found {
            scanning_error_rate += value;
        }
    }
    scanning_error_rate
}

fn run_part2(rules: &[Rule], your_ticket: &Ticket, nearby_tickets: &[Ticket]) -> u64 {
    let valid_tickets: Vec<&Ticket> = nearby_tickets.iter().filter(|&t| is_ticket_valid(t, rules)).collect();

    let mut rules_left = VecDeque::from_iter(rules);
    let mut fields_left: HashSet<usize> = (0..rules.len()).collect();
    let mut rules_mapping: HashMap<usize, &Rule> = HashMap::new();

    while !rules_left.is_empty() {
        let rule = rules_left.pop_front().unwrap();
        let mut matching_fields = fields_left.clone();
        for ticket in valid_tickets.iter() {
            let matching_fields_for_ticket: HashSet<_> = fields_left
                .iter()
                .filter(|&f| rule.matches(ticket.values[*f]))
                .cloned()
                .collect();
            matching_fields = &matching_fields & &matching_fields_for_ticket;
        }

        if matching_fields.len() == 1 {
            let matching_field = *matching_fields.iter().next().unwrap();
            fields_left.remove(&matching_field);
            rules_mapping.insert(matching_field, rule);
        } else {
            rules_left.push_back(rule);
        }
    }

    rules_mapping
        .iter()
        .filter(|(_, v)| v.name.starts_with("departure"))
        .map(|(k, _)| your_ticket.values[*k] as u64)
        .product()
}

fn is_ticket_valid(ticket: &Ticket, rules: &[Rule]) -> bool {
    for ticket_value in ticket.values.iter() {
        let mut matching_rules = rules.iter().filter(|&r| r.matches(*ticket_value));
        if matching_rules.next().is_none() {
            return false;
        }
    }
    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let (rules, your_ticket, nearby_tickets) = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&rules, &nearby_tickets), 71);
        assert_eq!(run_part2(&rules, &your_ticket, &nearby_tickets), 1);
    }
}

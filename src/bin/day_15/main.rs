use anyhow::{ensure, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::hash_map::Entry;
use std::collections::HashMap;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<u32>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    lazy_static! {
        static ref RE: Regex = Regex::new(r"^\d+(,\d+)*$").unwrap();
    }

    let result: Result<Vec<_>, _> = input
        .lines()
        .next()
        .unwrap()
        .split(",")
        .map(|s| s.parse::<u32>())
        .collect();
    result.context("Failed to parse input")
}

fn run_part1(input: &[u32]) -> u32 {
    let mut numbers = input.to_vec();
    while numbers.len() < 2020 {
        let last_number = numbers[numbers.len() - 1];
        let mut maybe_found_pos = None;
        for i in (0..numbers.len() - 1).rev() {
            if numbers[i] == last_number {
                maybe_found_pos = Some(i);
                break;
            }
        }
        numbers.push(match maybe_found_pos {
            Some(pos) => (numbers.len() - 1 - pos) as u32,
            None => 0,
        });
    }
    *numbers.last().unwrap()
}

fn run_part2(input: &[u32]) -> u32 {
    let mut numbers: HashMap<u32, usize> = HashMap::new();
    for (pos, &n) in input[0..input.len() - 1].iter().enumerate() {
        numbers.insert(n, pos);
    }
    let mut last_number = input[input.len() - 1];
    for pos in input.len()..30000000 {
        let entry = numbers.entry(last_number);
        last_number = match entry {
            Entry::Occupied(mut o) => (pos - 1 - o.insert(pos - 1)) as u32,
            Entry::Vacant(v) => {
                v.insert(pos - 1);
                0
            }
        };
    }
    last_number
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_examples1() {
        assert_eq!(run_part1(&vec![0, 3, 6]), 436);
        assert_eq!(run_part1(&vec![1, 3, 2]), 1);
        assert_eq!(run_part1(&vec![2, 1, 3]), 10);
        assert_eq!(run_part1(&vec![1, 2, 3]), 27);
        assert_eq!(run_part1(&vec![2, 3, 1]), 78);
        assert_eq!(run_part1(&vec![3, 2, 1]), 438);
        assert_eq!(run_part1(&vec![3, 1, 2]), 1836);
    }

    #[test]
    fn test_examples2() {
        assert_eq!(run_part2(&vec![0, 3, 6]), 175594);
        assert_eq!(run_part2(&vec![1, 3, 2]), 2578);
        assert_eq!(run_part2(&vec![2, 1, 3]), 3544142);
        assert_eq!(run_part2(&vec![1, 2, 3]), 261214);
        assert_eq!(run_part2(&vec![2, 3, 1]), 6895259);
        assert_eq!(run_part2(&vec![3, 2, 1]), 18);
        assert_eq!(run_part2(&vec![3, 1, 2]), 362);
    }
}

use std::iter;

use anyhow::{bail, ensure, Result};
use lazy_static::lazy_static;
use regex::Regex;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<Vec<u32>>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    lazy_static! {
        static ref RE: Regex = Regex::new(r"^[a-z]+$").unwrap();
    }

    let mut surveys_by_group = Vec::new();
    let mut surveys = Vec::new();
    for line in input.lines().chain(iter::once("")) {
        if line.is_empty() {
            if !surveys.is_empty() {
                surveys_by_group.push(surveys);
                surveys = Vec::new();
            }
        } else {
            if !RE.is_match(line) {
                bail!("Failed to parse line in input: '{}'", line);
            }
            let mut survey_coding = 0;
            for c in line.chars() {
                let answer_num = c as u32 - 'a' as u32;
                survey_coding |= 1 << answer_num;
            }
            surveys.push(survey_coding);
        }
    }

    Ok(surveys_by_group)
}

fn run_part1(surveys_by_group: &[Vec<u32>]) -> u32 {
    let mut summarised_surveys = Vec::new();
    for surveys in surveys_by_group {
        let mut summarised_survey_coding = 0;
        for survey_coding in surveys {
            summarised_survey_coding |= survey_coding;
        }
        summarised_surveys.push(summarised_survey_coding);
    }

    summarised_surveys.iter().map(|&sc| sc.count_ones()).sum()
}

fn run_part2(surveys_by_group: &[Vec<u32>]) -> u32 {
    let mut summarised_surveys = Vec::new();
    for surveys in surveys_by_group {
        let mut summarised_survey_coding = u32::MAX;
        for survey_coding in surveys {
            summarised_survey_coding &= survey_coding;
        }
        summarised_surveys.push(summarised_survey_coding);
    }

    summarised_surveys.iter().map(|&sc| sc.count_ones()).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input), 11);
        assert_eq!(run_part2(&input), 6);
    }
}

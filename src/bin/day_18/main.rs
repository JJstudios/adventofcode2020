use anyhow::{bail, ensure, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Clone, Copy, Debug, PartialEq)]
enum Token {
    Operand(u32),
    OperatorAdd,
    OperatorMul,
    BracketOpen,
    BracketClose,
}

type Equation = Vec<Token>;
type ReversePolishEquation = Vec<Token>;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<Equation>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<_>> = input.lines().map(|l| parse_line(l)).collect();
    result.context("Failed to parse input")
}

fn parse_line(line: &str) -> Result<Equation> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^[1-9 \+\*\(\)]+$").unwrap();
    }

    if !RE.is_match(line) {
        bail!("Mismatched line found in input: '{}'", line);
    }

    Ok(line.chars().filter(|&c| c != ' ').map(|c| parse_token(c)).collect())
}

fn parse_token(c: char) -> Token {
    return match c {
        '+' => Token::OperatorAdd,
        '*' => Token::OperatorMul,
        '(' => Token::BracketOpen,
        ')' => Token::BracketClose,
        '1'..='9' => Token::Operand(c.to_digit(10).unwrap()),
        _ => unreachable!(),
    };
}

fn evaluate_reverse_polish(rp_equation: &ReversePolishEquation) -> Result<u64> {
    let mut stack = vec![];
    for token in rp_equation.iter() {
        match token {
            &Token::OperatorAdd => {
                if stack.len() < 2 {
                    bail!("Stack too short");
                };
                let a = stack.pop().unwrap();
                let b = stack.pop().unwrap();
                stack.push(a + b);
            }
            &Token::OperatorMul => {
                if stack.len() < 2 {
                    bail!("Stack too short");
                };
                let a = stack.pop().unwrap();
                let b = stack.pop().unwrap();
                stack.push(a * b);
            }
            &Token::Operand(n) => stack.push(n as u64),
            _ => unreachable!(),
        }
    }
    if stack.len() != 1 {
        bail!("Stack doesn't have 1 element: {}", stack.len());
    }
    Ok(stack[0])
}

fn run_part1(equations: &[Equation]) -> Result<u64> {
    let results_rp_equations: Result<Vec<_>> = equations.iter().map(|e| convert_to_reverse_polish_part1(e)).collect();
    let rp_equations = results_rp_equations.context("Failed to convert equations to RP")?;
    let results_evaluations: Result<Vec<_>> = rp_equations.iter().map(|e| evaluate_reverse_polish(e)).collect();
    let evaluations = results_evaluations.context("Failed to evaluate RPs")?;
    Ok(evaluations.iter().sum())
}

fn convert_to_reverse_polish_part1(equation: &Equation) -> Result<ReversePolishEquation> {
    let mut rp_equation = vec![];
    let mut stack = vec![];
    for &token in equation.iter() {
        match token {
            Token::OperatorAdd | Token::OperatorMul => {
                if stack.len() > 0 && stack[stack.len() - 1] != Token::BracketOpen {
                    rp_equation.push(stack.pop().unwrap());
                }
                stack.push(token);
            }
            Token::BracketOpen => {
                stack.push(token);
            }
            Token::BracketClose => {
                loop {
                    if stack.len() == 0 {
                        bail!("Something has gone wrong");
                    }
                    if stack[stack.len() - 1] == Token::BracketOpen {
                        break;
                    }
                    rp_equation.push(stack.pop().unwrap());
                }
                stack.pop();
            }
            Token::Operand(_) => {
                rp_equation.push(token);
            }
        }
    }
    while !stack.is_empty() {
        rp_equation.push(stack.pop().unwrap());
    }
    Ok(rp_equation)
}

fn run_part2(equations: &[Equation]) -> Result<u64> {
    let results_rp_equations: Result<Vec<_>> = equations.iter().map(|e| convert_to_reverse_polish_part2(e)).collect();
    let rp_equations = results_rp_equations.context("Failed to convert equations to RP")?;
    let results_evaluations: Result<Vec<_>> = rp_equations.iter().map(|e| evaluate_reverse_polish(e)).collect();
    let evaluations = results_evaluations.context("Failed to evaluate RPs")?;
    Ok(evaluations.iter().sum())
}

fn convert_to_reverse_polish_part2(equation: &Equation) -> Result<ReversePolishEquation> {
    let mut rp_equation = vec![];
    let mut stack = vec![];
    for &token in equation.iter() {
        match token {
            Token::OperatorAdd => {
                if stack.len() > 0 && stack[stack.len() - 1] == Token::OperatorAdd {
                    rp_equation.push(stack.pop().unwrap());
                }
                stack.push(token);
            }
            Token::OperatorMul => {
                if stack.len() > 0 && stack[stack.len() - 1] != Token::BracketOpen {
                    rp_equation.push(stack.pop().unwrap());
                }
                stack.push(token);
            }
            Token::BracketOpen => {
                stack.push(token);
            }
            Token::BracketClose => {
                loop {
                    if stack.len() == 0 {
                        bail!("Something has gone wrong");
                    }
                    if stack[stack.len() - 1] == Token::BracketOpen {
                        break;
                    }
                    rp_equation.push(stack.pop().unwrap());
                }
                stack.pop();
            }
            Token::Operand(_) => {
                rp_equation.push(token);
            }
        }
    }
    while !stack.is_empty() {
        rp_equation.push(stack.pop().unwrap());
    }
    Ok(rp_equation)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input).unwrap(), 26457);
        assert_eq!(run_part2(&input).unwrap(), 694173);
    }

    #[test]
    fn test_parse() {
        let equation = parse_input("1 + (2 * 3)").unwrap();

        assert_eq!(equation.len(), 1);
        assert_eq!(equation[0].len(), 7);
        assert_eq!(equation[0][0], Token::Operand(1));
        assert_eq!(equation[0][1], Token::OperatorAdd);
        assert_eq!(equation[0][2], Token::BracketOpen);
        assert_eq!(equation[0][3], Token::Operand(2));
        assert_eq!(equation[0][4], Token::OperatorMul);
        assert_eq!(equation[0][5], Token::Operand(3));
        assert_eq!(equation[0][6], Token::BracketClose);
    }

    #[test]
    fn test_evaluate_reverse_polish() {
        let reverse_polish = vec![
            Token::Operand(1),
            Token::Operand(2),
            Token::OperatorAdd,
            Token::Operand(3),
            Token::OperatorMul,
        ];
        let answer = evaluate_reverse_polish(&reverse_polish).unwrap();

        assert_eq!(answer, 9);
    }

    #[test]
    fn test_convert_to_reverse_polish_part1() {
        let equation = vec![
            Token::Operand(1),
            Token::OperatorMul,
            Token::Operand(2),
            Token::OperatorAdd,
            Token::Operand(3),
        ];
        let reverse_polish = convert_to_reverse_polish_part1(&equation).unwrap();

        assert_eq!(reverse_polish.len(), 5);
        assert_eq!(reverse_polish[0], Token::Operand(1));
        assert_eq!(reverse_polish[1], Token::Operand(2));
        assert_eq!(reverse_polish[2], Token::OperatorMul);
        assert_eq!(reverse_polish[3], Token::Operand(3));
        assert_eq!(reverse_polish[4], Token::OperatorAdd);
    }

    #[test]
    fn test_convert_to_reverse_polish_part1_with_brackets() {
        let equation = vec![
            Token::Operand(1),
            Token::OperatorMul,
            Token::BracketOpen,
            Token::Operand(2),
            Token::OperatorAdd,
            Token::Operand(3),
            Token::BracketClose,
        ];
        let reverse_polish = convert_to_reverse_polish_part1(&equation).unwrap();

        assert_eq!(reverse_polish.len(), 5);
        assert_eq!(reverse_polish[0], Token::Operand(1));
        assert_eq!(reverse_polish[1], Token::Operand(2));
        assert_eq!(reverse_polish[2], Token::Operand(3));
        assert_eq!(reverse_polish[3], Token::OperatorAdd);
        assert_eq!(reverse_polish[4], Token::OperatorMul);
    }

    #[test]
    fn test_convert_to_reverse_polish_part2() {
        let equation = vec![
            Token::Operand(1),
            Token::OperatorMul,
            Token::Operand(2),
            Token::OperatorAdd,
            Token::Operand(3),
        ];
        let reverse_polish = convert_to_reverse_polish_part2(&equation).unwrap();

        assert_eq!(reverse_polish.len(), 5);
        assert_eq!(reverse_polish[0], Token::Operand(1));
        assert_eq!(reverse_polish[1], Token::Operand(2));
        assert_eq!(reverse_polish[2], Token::Operand(3));
        assert_eq!(reverse_polish[3], Token::OperatorAdd);
        assert_eq!(reverse_polish[4], Token::OperatorMul);
    }

    #[test]
    fn test_convert_to_reverse_polish_part2_with_brackets() {
        let equation = vec![
            Token::BracketOpen,
            Token::Operand(1),
            Token::OperatorMul,
            Token::Operand(2),
            Token::BracketClose,
            Token::OperatorAdd,
            Token::Operand(3),
        ];
        let reverse_polish = convert_to_reverse_polish_part1(&equation).unwrap();

        assert_eq!(reverse_polish.len(), 5);
        assert_eq!(reverse_polish[0], Token::Operand(1));
        assert_eq!(reverse_polish[1], Token::Operand(2));
        assert_eq!(reverse_polish[2], Token::OperatorMul);
        assert_eq!(reverse_polish[3], Token::Operand(3));
        assert_eq!(reverse_polish[4], Token::OperatorAdd);
    }
}

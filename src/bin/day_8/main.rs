use anyhow::{bail, ensure, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Clone)]
enum Instruction {
    Acc(i32),
    Jmp(i32),
    Nop(i32),
}

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input)?);
    println!("Part 2: {:?}", run_part2(&input)?);

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<Instruction>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(acc|jmp|nop) ([+\-]\d+)$").unwrap();
    }

    let mut instructions = Vec::new();
    for line in input.lines() {
        let caps = RE
            .captures(line)
            .with_context(|| format!("Failed to parse line in input: '{}'", line))?;
        let instruction_payload = caps[2].parse().unwrap();
        let instruction = match &caps[1] {
            "acc" => Instruction::Acc(instruction_payload),
            "jmp" => Instruction::Jmp(instruction_payload),
            "nop" => Instruction::Nop(instruction_payload),
            _ => unreachable!(),
        };
        instructions.push(instruction);
    }

    Ok(instructions)
}

fn run_part1(instructions: &[Instruction]) -> Result<i32> {
    let mut program_counter: usize = 0;
    let mut accumulator: i32 = 0;
    let mut executeds = vec![false; instructions.len()];

    loop {
        if executeds[program_counter] {
            return Ok(accumulator);
        }
        executeds[program_counter] = true;

        // TODO: Is the & good or pointless here?
        match &instructions[program_counter] {
            &Instruction::Acc(n) => {
                accumulator += n;
                program_counter += 1;
            }
            &Instruction::Jmp(n) => {
                let jump_target = program_counter as i32 + n;
                if jump_target < 0 || jump_target > instructions.len() as i32 {
                    bail!("Program has bad jump");
                }
                program_counter = jump_target as usize;
            }
            &Instruction::Nop(_) => {
                program_counter += 1;
            }
        }

        if program_counter == instructions.len() {
            bail!("Program terminated");
        }
    }
}

fn run_part2(instructions: &[Instruction]) -> Result<i32> {
    for i in 0..instructions.len() {
        let mut new_instructions: Vec<_> = instructions.to_vec();
        match new_instructions[i] {
            Instruction::Jmp(n) => {
                new_instructions[i] = Instruction::Nop(n);
                if let Some(a) = check_program_for_part2(&new_instructions) {
                    return Ok(a);
                }
            }
            Instruction::Nop(n) => {
                new_instructions[i] = Instruction::Jmp(n);
                if let Some(a) = check_program_for_part2(&new_instructions) {
                    return Ok(a);
                }
            }
            _ => (),
        }
    }

    bail!("Program correction failed");
}

fn check_program_for_part2(instructions: &[Instruction]) -> Option<i32> {
    let mut program_counter: usize = 0;
    let mut accumulator: i32 = 0;
    let mut executeds = vec![false; instructions.len()];

    loop {
        if executeds[program_counter] {
            return None;
        }
        executeds[program_counter] = true;

        match &instructions[program_counter] {
            &Instruction::Acc(n) => {
                accumulator += n;
                program_counter += 1;
            }
            &Instruction::Jmp(n) => {
                let jump_target = program_counter as i32 + n;
                if jump_target < 0 || jump_target > instructions.len() as i32 {
                    return None;
                }
                program_counter = jump_target as usize;
            }
            &Instruction::Nop(_) => {
                program_counter += 1;
            }
        }

        if program_counter == instructions.len() {
            return Some(accumulator);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input).unwrap(), 5);
        assert_eq!(run_part2(&input).unwrap(), 8);
    }
}

use anyhow::{ensure, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;

enum Instruction {
    Mask(Mask),
    Mem(u64, u64),
}

#[derive(Clone, Copy)]
struct Mask {
    zeroes: u64,
    ones: u64,
    exes: u64,
}

impl Mask {
    pub fn new() -> Mask {
        Mask {
            zeroes: u64::MAX,
            ones: 0,
            exes: 0,
        }
    }

    pub fn parse(input: &str) -> Mask {
        let mut mask = Mask::new();
        for (pos, c) in input.chars().rev().enumerate() {
            match c {
                '0' => mask.zeroes &= u64::MAX - (1 << pos),
                '1' => mask.ones |= 1 << pos,
                'X' => mask.exes |= 1 << pos,
                _ => unreachable!(),
            }
        }
        mask
    }

    pub fn apply_part1(&self, value: u64) -> u64 {
        value & self.zeroes | self.ones
    }

    pub fn apply_part2(&self, location: u64) -> Vec<u64> {
        let mut locations = Vec::new();
        locations.push(location | self.ones | self.exes);
        for pos in 0..36 {
            if (self.exes >> pos) & 1 == 1 {
                let num_locations = locations.len();
                for n in 0..num_locations {
                    locations.push(locations[n] & (u64::MAX - (1 << pos)));
                }
            }
        }
        locations
    }
}

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<Instruction>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(mask = ([01X]{36}))|(mem\[(\d+)\] = (\d+))$").unwrap();
    }

    let mut instructions = Vec::new();
    for line in input.lines() {
        let caps = RE
            .captures(line)
            .with_context(|| format!("Failed to parse line in input: '{}'", line))?;

        let instruction = if caps.get(1).is_some() {
            Instruction::Mask(Mask::parse(&caps[2]))
        } else {
            Instruction::Mem(caps[4].parse::<u64>().unwrap(), caps[5].parse::<u64>().unwrap())
        };
        instructions.push(instruction);
    }

    Ok(instructions)
}

fn run_part1(instructions: &[Instruction]) -> u64 {
    let mut current_mask = Mask::new();
    let mut memory = HashMap::new();
    for instruction in instructions {
        match instruction {
            Instruction::Mask(mask) => current_mask = *mask,
            Instruction::Mem(location, value) => {
                memory.insert(*location, current_mask.apply_part1(*value));
            }
        }
    }
    memory.values().map(|&v| v).sum()
}

fn run_part2(instructions: &[Instruction]) -> u64 {
    let mut current_mask = Mask::new();
    let mut memory = HashMap::new();
    for instruction in instructions {
        match instruction {
            Instruction::Mask(mask) => current_mask = *mask,
            Instruction::Mem(location, value) => {
                for loc in current_mask.apply_part2(*location) {
                    memory.insert(loc, *value);
                }
            }
        }
    }
    memory.values().map(|&v| v).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = parse_input(include_str!("input_example1")).unwrap();

        assert_eq!(run_part1(&input), 165);
    }

    #[test]
    fn test_example2() {
        let input = parse_input(include_str!("input_example2")).unwrap();

        assert_eq!(run_part2(&input), 208);
    }
}

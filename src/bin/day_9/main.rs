use anyhow::{bail, ensure, Context, Result};

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    let answer_part1 = run_part1(&input, 25)?;
    println!("Part 1: {:?}", answer_part1);
    println!("Part 2: {:?}", run_part2(&input, answer_part1)?);

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<u64>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<_>, _> = input.lines().map(|l| l.parse::<u64>()).collect();
    result.context("Failed to parse input")
}

fn run_part1(data: &[u64], window: usize) -> Result<u64> {
    'outer: for target_index in window..data.len() {
        let target = data[target_index];
        for i in target_index - window..target_index - 1 {
            for j in i + 1..target_index {
                if data[i] + data[j] == target {
                    continue 'outer;
                }
            }
        }
        return Ok(target);
    }

    bail!("No answer found");
}

fn run_part2(data: &[u64], target_sum: u64) -> Result<u64> {
    if let Some((a, b)) = find_range_for_part2(data, target_sum) {
        let range = &data[a..=b];
        return Ok(range.iter().min().unwrap() + range.iter().max().unwrap());
    }

    bail!("No answer found");
}

fn find_range_for_part2(data: &[u64], target_sum: u64) -> Option<(usize, usize)> {
    for start_index in 0..data.len() - 1 {
        let mut sum = data[start_index];
        for end_index in start_index + 1..data.len() {
            sum += data[end_index];
            if sum == target_sum {
                return Some((start_index, end_index));
            }
            if sum > target_sum {
                break;
            }
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input, 5).unwrap(), 127);
        assert_eq!(run_part2(&input, 127).unwrap(), 62);
    }
}

use anyhow::{bail, ensure, Context, Result};
use array2d::Array2D;
use lazy_static::lazy_static;
use regex::Regex;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Array2D<bool>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<Vec<_>>, _> = input.lines().map(|l| parse_line(l)).collect();
    let parsed_lines = result.context("Failed to parse input")?;

    let line_len = parsed_lines[0].len();
    if !parsed_lines.iter().all(|l| l.len() == line_len) {
        bail!("Input lines have mismatched lengths");
    }

    Ok(Array2D::from_rows(&parsed_lines))
}

fn parse_line(line: &str) -> Result<Vec<bool>> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^[\.L]+$").unwrap();
    }

    if !RE.is_match(line) {
        bail!("Mismatched line found in input: '{}'", line);
    }

    Ok(line.chars().map(|c| c == 'L').collect())
}

fn run_part1(array: &Array2D<bool>) -> usize {
    // Create a 1-based list of all available chairs
    let mut chairs = Vec::new();
    for r in 0..array.num_rows() {
        for c in 0..array.num_columns() {
            if array[(r, c)] {
                chairs.push((r + 1, c + 1));
            }
        }
    }
    let chairs = chairs;

    // Store state in an array with a border of one
    let mut state = Array2D::filled_with(false, array.num_rows() + 2, array.num_columns() + 2);
    loop {
        let mut state_changed = false;
        let mut new_state = state.clone();
        for &chair in chairs.iter() {
            if state[chair] {
                if get_neighbours_part1(chair).iter().filter(|&&n| state[n]).count() >= 4 {
                    new_state[chair] = false;
                    state_changed = true;
                }
            } else {
                if get_neighbours_part1(chair).iter().all(|&n| !state[n]) {
                    new_state[chair] = true;
                    state_changed = true;
                }
            }
        }
        if !state_changed {
            break;
        }
        state = new_state;
    }

    state.elements_row_major_iter().filter(|&&c| c).count()
}

fn get_neighbours_part1(chair: (usize, usize)) -> Vec<(usize, usize)> {
    let (r, c) = chair;
    vec![
        (r - 1, c - 1),
        (r - 1, c),
        (r - 1, c + 1),
        (r, c - 1),
        (r, c + 1),
        (r + 1, c - 1),
        (r + 1, c),
        (r + 1, c + 1),
    ]
}

fn run_part2(array: &Array2D<bool>) -> usize {
    let mut chairs = Vec::new();
    for r in 0..array.num_rows() {
        for c in 0..array.num_columns() {
            if array[(r, c)] {
                chairs.push((r, c));
            }
        }
    }
    let chairs = chairs;

    let mut state = Array2D::filled_with(false, array.num_rows(), array.num_columns());
    loop {
        let mut state_changed = false;
        let mut new_state = state.clone();
        for &chair in chairs.iter() {
            if state[chair] {
                if get_neighbours_part2(chair, &array)
                    .iter()
                    .filter(|&&n| state[n])
                    .count()
                    >= 5
                {
                    new_state[chair] = false;
                    state_changed = true;
                }
            } else {
                if get_neighbours_part2(chair, &array).iter().all(|&n| !state[n]) {
                    new_state[chair] = true;
                    state_changed = true;
                }
            }
        }
        if !state_changed {
            break;
        }
        state = new_state;
    }

    state.elements_row_major_iter().filter(|&&c| c).count()
}

fn get_neighbours_part2(chair: (usize, usize), chairs: &Array2D<bool>) -> Vec<(usize, usize)> {
    let num_rows = chairs.num_rows() as i32;
    let num_columns = chairs.num_columns() as i32;

    let directions = vec![(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)];

    let mut neighbours = Vec::new();
    for direction in directions {
        let (dr, dc) = direction;
        let mut r = chair.0 as i32;
        let mut c = chair.1 as i32;
        loop {
            r += dr;
            c += dc;
            if r < 0 || c < 0 || r >= num_rows || c >= num_columns {
                break;
            }
            let test_chair = (r as usize, c as usize);
            if chairs[test_chair] {
                neighbours.push(test_chair);
                break;
            }
        }
    }

    neighbours
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input), 37);
        assert_eq!(run_part2(&input), 26);
    }
}

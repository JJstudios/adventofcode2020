use anyhow::{bail, ensure, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;

enum Instruction {
    North(u16),
    South(u16),
    East(u16),
    West(u16),
    Left(u16),
    Right(u16),
    Forward(u16),
}

struct Position {
    lat: i32,
    lon: i32,
}

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<Instruction>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    lazy_static! {
        static ref RE: Regex = Regex::new(r"^([NSEWLRF])(\d+)$").unwrap();
    }

    let mut instructions = Vec::new();
    for line in input.lines() {
        let caps = RE
            .captures(line)
            .with_context(|| format!("Failed to parse line in input: '{}'", line))?;
        let instruction_payload = caps[2].parse().unwrap();
        if &caps[1] == "L" || &caps[1] == "R" {
            if instruction_payload != 90 && instruction_payload != 180 && instruction_payload != 270 {
                bail!("Bad heading change found in input: '{}'", line);
            }
        }
        let instruction = match &caps[1] {
            "N" => Instruction::North(instruction_payload),
            "S" => Instruction::South(instruction_payload),
            "E" => Instruction::East(instruction_payload),
            "W" => Instruction::West(instruction_payload),
            "L" => Instruction::Left(instruction_payload),
            "R" => Instruction::Right(instruction_payload),
            "F" => Instruction::Forward(instruction_payload),
            &_ => unreachable!(),
        };
        instructions.push(instruction);
    }

    Ok(instructions)
}

fn run_part1(instructions: &[Instruction]) -> u32 {
    let mut position: Position = Position { lat: 0, lon: 0 };
    let mut heading: u16 = 90; // Start off facing east

    for instruction in instructions {
        match instruction {
            &Instruction::North(units) => position.lat += units as i32,
            &Instruction::South(units) => position.lat -= units as i32,
            &Instruction::East(units) => position.lon += units as i32,
            &Instruction::West(units) => position.lon -= units as i32,
            &Instruction::Left(degrees) => {
                if heading < degrees {
                    heading += 360
                }
                heading -= degrees;
            }
            &Instruction::Right(degrees) => {
                heading += degrees;
                if heading >= 360 {
                    heading -= 360
                }
            }
            &Instruction::Forward(units) => match heading {
                0 => position.lat += units as i32,
                90 => position.lon += units as i32,
                180 => position.lat -= units as i32,
                270 => position.lon -= units as i32,
                _ => unreachable!(),
            },
        }
    }

    (position.lat.abs() + position.lon.abs()) as u32
}

fn run_part2(instructions: &[Instruction]) -> u32 {
    let mut position: Position = Position { lat: 0, lon: 0 };
    let mut waypoint: Position = Position { lat: 1, lon: 10 };

    for instruction in instructions {
        match instruction {
            &Instruction::North(units) => waypoint.lat += units as i32,
            &Instruction::South(units) => waypoint.lat -= units as i32,
            &Instruction::East(units) => waypoint.lon += units as i32,
            &Instruction::West(units) => waypoint.lon -= units as i32,
            &Instruction::Left(degrees) => waypoint = rotate_waypoint(waypoint, 360 - degrees),

            &Instruction::Right(degrees) => waypoint = rotate_waypoint(waypoint, degrees),
            &Instruction::Forward(units) => {
                position.lat += waypoint.lat * units as i32;
                position.lon += waypoint.lon * units as i32;
            }
        }
    }

    (position.lat.abs() + position.lon.abs()) as u32
}

fn rotate_waypoint(waypoint: Position, degrees: u16) -> Position {
    match degrees {
        90 => Position {
            lat: -waypoint.lon,
            lon: waypoint.lat,
        },
        180 => Position {
            lat: -waypoint.lat,
            lon: -waypoint.lon,
        },
        270 => Position {
            lat: waypoint.lon,
            lon: -waypoint.lat,
        },
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input), 25);
        assert_eq!(run_part2(&input), 286);
    }
}

use anyhow::{bail, ensure, Context, Result};
use array2d::Array2D;
use lazy_static::lazy_static;
use regex::Regex;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Array2D<bool>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<Vec<_>>, _> = input.lines().map(|l| parse_line(l)).collect();
    let parsed_lines = result.context("Failed to parse input")?;

    let line_len = parsed_lines[0].len();
    if !parsed_lines.iter().all(|l| l.len() == line_len) {
        bail!("Input lines have mismatched lengths");
    }

    Ok(Array2D::from_rows(&parsed_lines))
}

fn parse_line(line: &str) -> Result<Vec<bool>> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^[\.#]+$").unwrap();
    }

    if !RE.is_match(line) {
        bail!("Mismatched line found in input: '{}'", line);
    }

    Ok(line.chars().map(|c| c == '#').collect())
}

fn run_part1(array: &Array2D<bool>) -> usize {
    toboggan(array, 3, 1)
}

fn run_part2(array: &Array2D<bool>) -> usize {
    toboggan(array, 1, 1) * toboggan(array, 3, 1) * toboggan(array, 5, 1) * toboggan(array, 7, 1) * toboggan(array, 1, 2)
}

fn toboggan(array: &Array2D<bool>, right: usize, down: usize) -> usize {
    let mut row = 0;
    let mut col = 0;
    let mut num_trees = 0;
    while row < array.num_rows() {
        if array[(row, col % array.num_columns())] {
            num_trees += 1;
        }
        row += down;
        col += right;
    }
    num_trees
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input), 7);
        assert_eq!(run_part2(&input), 336);
    }
}

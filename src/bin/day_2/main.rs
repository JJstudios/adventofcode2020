use anyhow::{ensure, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;

struct Entry<'a> {
    num1: usize,
    num2: usize,
    character: char,
    password: &'a str,
}

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<Entry>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<_>, _> = input.lines().map(|l| parse_line(l)).collect();
    result.context("Failed to parse input")
}

fn parse_line(line: &str) -> Result<Entry> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\d+)-(\d+) ([a-z]): ([a-z]+)$").unwrap();
    }
    let caps = RE
        .captures(line)
        .context(format!("Mismatched line found in input: '{}'", line))?;

    Ok(Entry {
        num1: caps[1].parse::<usize>().unwrap(),
        num2: caps[2].parse::<usize>().unwrap(),
        character: caps[3].chars().nth(0).unwrap(),
        password: &line[line.len() - caps[4].len()..],
    })
}

fn run_part1(data: &[Entry]) -> usize {
    data.iter().filter(|&t| check_line_part1(t)).count()
}

fn check_line_part1(entry: &Entry) -> bool {
    let (min, max) = (entry.num1, entry.num2);
    let count = entry.password.chars().filter(|&c| c == entry.character).count();
    count >= min && count <= max
}

fn run_part2(data: &[Entry]) -> usize {
    data.iter().filter(|&t| check_line_part2(t)).count()
}

fn check_line_part2(entry: &Entry) -> bool {
    let (pos_a, pos_b) = (entry.num1, entry.num2);

    if pos_a > entry.password.len() || pos_b > entry.password.len() {
        return false;
    }

    let is_char_at_pos_a = entry.password.chars().nth(pos_a - 1).unwrap() == entry.character;
    let is_char_at_pos_b = entry.password.chars().nth(pos_b - 1).unwrap() == entry.character;
    is_char_at_pos_a != is_char_at_pos_b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input), 2);
        assert_eq!(run_part2(&input), 1);
    }
}

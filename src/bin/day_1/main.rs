use anyhow::{bail, ensure, Context, Result};

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input)?);
    println!("Part 2: {:?}", run_part2(&input)?);

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<u32>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<_>, _> = input.lines().map(|l| l.parse::<u32>()).collect();
    result.context("Failed to parse input")
}

fn run_part1(data: &[u32]) -> Result<u32> {
    let data_count = data.len();
    for i in 0..(data_count - 1) {
        for j in (i + 1)..data_count {
            if data[i] + data[j] == 2020 {
                return Ok(data[i] * data[j]);
            }
        }
    }
    bail!("No solution exists");
}

fn run_part2(data: &[u32]) -> Result<u32> {
    let data_count = data.len();
    for i in 0..(data_count - 2) {
        for j in (i + 1)..(data_count - 1) {
            for k in (j + 1)..data_count {
                if data[i] + data[j] + data[k] == 2020 {
                    return Ok(data[i] * data[j] * data[k]);
                }
            }
        }
    }
    bail!("No solution exists");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input).unwrap(), 514579);
        assert_eq!(run_part2(&input).unwrap(), 241861950);
    }
}

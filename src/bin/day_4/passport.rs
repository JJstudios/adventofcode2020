use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashMap;

pub struct Passport {
    map: HashMap<String, String>,
}

impl Passport {
    pub fn new(map: HashMap<String, String>) -> Self {
        Self { map }
    }

    pub fn has_reqd_fields(&self) -> bool {
        self.map.contains_key("byr")
            && self.map.contains_key("iyr")
            && self.map.contains_key("eyr")
            && self.map.contains_key("hgt")
            && self.map.contains_key("hcl")
            && self.map.contains_key("ecl")
            && self.map.contains_key("pid")
    }

    pub fn is_valid(&self) -> bool {
        self.has_reqd_fields()
            && is_valid_byr(&self.map["byr"])
            && is_valid_iyr(&self.map["iyr"])
            && is_valid_eyr(&self.map["eyr"])
            && is_valid_hgt(&self.map["hgt"])
            && is_valid_hcl(&self.map["hcl"])
            && is_valid_ecl(&self.map["ecl"])
            && is_valid_pid(&self.map["pid"])
    }
}

fn is_valid_byr(byr: &str) -> bool {
    is_valid_yr(byr, 1920, 2002)
}

fn is_valid_iyr(iyr: &str) -> bool {
    is_valid_yr(iyr, 2010, 2020)
}

fn is_valid_eyr(eyr: &str) -> bool {
    is_valid_yr(eyr, 2020, 2030)
}

fn is_valid_yr(yr: &str, min: u32, max: u32) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^\d{4}$").unwrap();
    }

    if !RE.is_match(yr) {
        return false;
    }
    let yr_num = yr.parse::<u32>().unwrap();
    yr_num >= min && yr_num <= max
}

fn is_valid_hgt(hgt: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^(\d+)(cm|in)$").unwrap();
    }

    if !RE.is_match(hgt) {
        return false;
    }

    let hgt_num = hgt[..hgt.len() - 2].parse::<u32>().unwrap();
    let hgt_units = &hgt[hgt.len() - 2..];
    if hgt_units == "cm" {
        hgt_num >= 150 && hgt_num <= 193
    } else {
        hgt_num >= 59 && hgt_num <= 76
    }
}

fn is_valid_hcl(hcl: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    }

    RE.is_match(hcl)
}

fn is_valid_ecl(ecl: &str) -> bool {
    ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&ecl)
}

fn is_valid_pid(pid: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^\d{9}$").unwrap();
    }

    RE.is_match(pid)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_has_required_fields() {
        let bad_passport = Passport::new(HashMap::new());
        assert_eq!(bad_passport.has_reqd_fields(), false);

        let good_passport = Passport::new(
            [
                (String::from("byr"), String::new()),
                (String::from("iyr"), String::new()),
                (String::from("eyr"), String::new()),
                (String::from("hgt"), String::new()),
                (String::from("hcl"), String::new()),
                (String::from("ecl"), String::new()),
                (String::from("pid"), String::new()),
            ]
            .iter()
            .cloned()
            .collect(),
        );
        assert_eq!(good_passport.has_reqd_fields(), true);
    }

    #[test]
    fn test_is_valid_byr() {
        assert_eq!(is_valid_byr("2002"), true);
        assert_eq!(is_valid_byr("2003"), false);
    }

    #[test]
    fn test_is_valid_hgt() {
        assert_eq!(is_valid_hgt("60in"), true);
        assert_eq!(is_valid_hgt("190cm"), true);
        assert_eq!(is_valid_hgt("190in"), false);
        assert_eq!(is_valid_hgt("190"), false);
    }

    #[test]
    fn test_is_valid_hcl() {
        assert_eq!(is_valid_hcl("#123abc"), true);
        assert_eq!(is_valid_hcl("#123abz"), false);
        assert_eq!(is_valid_hcl("123abc"), false);
    }

    #[test]
    fn test_is_valid_ecl() {
        assert_eq!(is_valid_ecl("brn"), true);
        assert_eq!(is_valid_ecl("wat"), false);
    }

    #[test]
    fn test_is_valid_pid() {
        assert_eq!(is_valid_pid("000000001"), true);
        assert_eq!(is_valid_pid("0123456789"), false);
    }
}

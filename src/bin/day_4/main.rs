mod passport;

use std::collections::HashMap;
use std::iter;

use anyhow::{ensure, Result};
use lazy_static::lazy_static;
use regex::Regex;

use passport::*;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<Passport>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    lazy_static! {
        static ref RE: Regex = Regex::new(r"([a-z]{3}):(\S+)").unwrap();
    }

    let mut passports = Vec::new();
    let mut dict = HashMap::new();
    for line in input.lines().chain(iter::once("")) {
        if line.is_empty() {
            if !dict.is_empty() {
                passports.push(Passport::new(dict));
                dict = HashMap::new();
            }
        } else {
            for caps in RE.captures_iter(line) {
                dict.insert(caps[1].to_string(), caps[2].to_string());
            }
        }
    }

    Ok(passports)
}

fn run_part1(passports: &[Passport]) -> usize {
    passports.iter().filter(|&p| p.has_reqd_fields()).count()
}

fn run_part2(passports: &[Passport]) -> usize {
    passports.iter().filter(|&p| p.is_valid()).count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = parse_input(include_str!("input_example1")).unwrap();

        assert_eq!(run_part1(&input), 2);
    }

    #[test]
    fn test_example2() {
        let input = parse_input(include_str!("input_example2")).unwrap();

        assert_eq!(run_part2(&input), 0);
    }

    #[test]
    fn test_example3() {
        let input = parse_input(include_str!("input_example3")).unwrap();

        assert_eq!(run_part2(&input), 4);
    }
}

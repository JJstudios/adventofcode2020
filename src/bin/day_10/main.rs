use anyhow::{bail, ensure, Context, Result};

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;
    let diffs = compute_diffs(&input)?;

    println!("Part 1: {:?}", run_part1(&diffs)?);
    println!("Part 2: {:?}", run_part2(&diffs)?);

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<u32>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<_>, _> = input.lines().map(|l| l.parse::<u32>()).collect();
    result.context("Failed to parse input")
}

fn compute_diffs(data: &[u32]) -> Result<Vec<u8>> {
    let mut sorted_data = data.to_vec();
    sorted_data.push(0);
    sorted_data.sort();
    sorted_data.push(sorted_data[sorted_data.len() - 1] + 3);
    let sorted_data = sorted_data;

    let mut diffs = Vec::new();
    for w in sorted_data.windows(2) {
        let diff = w[1] - w[0];
        if diff < 1 || diff > 3 {
            bail!("Impossible");
        }
        diffs.push(diff as u8);
    }

    Ok(diffs)
}

fn run_part1(diffs: &[u8]) -> Result<u32> {
    let num_ones = diffs.iter().filter(|&&d| d == 1).count() as u32;
    let num_threes = diffs.iter().filter(|&&d| d == 3).count() as u32;

    Ok(num_ones * num_threes)
}

fn run_part2(diffs: &[u8]) -> Result<u64> {
    // This array is # ways to get to current number, from [3, 2, 1] before respectively
    let ways = [1, 0, 0];
    let num_arrangements = diffs
        .iter()
        .fold(ways, |ways, &x| compute_new_ways(ways, x))
        .iter()
        .sum();
    Ok(num_arrangements)
}

fn compute_new_ways(old_ways: [u64; 3], next_diff: u8) -> [u64; 3] {
    match next_diff {
        1 => [old_ways[1], old_ways[2], old_ways.iter().sum()],
        2 => [old_ways[2], old_ways.iter().sum(), 0],
        3 => [old_ways.iter().sum(), 0, 0],
        _ => unreachable!(),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = parse_input(include_str!("input_example1")).unwrap();
        let diffs = compute_diffs(&input).unwrap();

        assert_eq!(run_part1(&diffs).unwrap(), 35);
        assert_eq!(run_part2(&diffs).unwrap(), 8);
    }

    #[test]
    fn test_example2() {
        let input = parse_input(include_str!("input_example2")).unwrap();
        let diffs = compute_diffs(&input).unwrap();

        assert_eq!(run_part1(&diffs).unwrap(), 220);
        assert_eq!(run_part2(&diffs).unwrap(), 19208);
    }
}

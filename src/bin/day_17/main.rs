use anyhow::{bail, ensure, Context, Result};
use lazy_static::lazy_static;
use multiarray::{Array2D, Array3D, Array4D};
use regex::Regex;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input));
    println!("Part 2: {:?}", run_part2(&input));

    Ok(())
}

fn parse_input(input: &str) -> Result<Array2D<bool>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<Vec<_>>, _> = input.lines().map(|l| parse_line(l)).collect();
    let parsed_lines = result.context("Failed to parse input")?;

    let line_len = parsed_lines[0].len();
    if !parsed_lines.iter().all(|l| l.len() == line_len) {
        bail!("Input lines have mismatched lengths");
    }

    let mut grid = Array2D::new([parsed_lines.len(), line_len], false);
    for y in 0..parsed_lines.len() {
        for x in 0..line_len {
            grid[[x, y]] = parsed_lines[y][x]
        }
    }

    Ok(grid)
}

fn parse_line(line: &str) -> Result<Vec<bool>> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^[\.#]+$").unwrap();
    }

    if !RE.is_match(line) {
        bail!("Mismatched line found in input: '{}'", line);
    }

    Ok(line.chars().map(|c| c == '#').collect())
}

fn run_part1(input: &Array2D<bool>) -> usize {
    let input_extents = input.extents();

    let mut grid = Array3D::new([input_extents[0], input_extents[1], 1], false);
    for x in 0..input_extents[0] {
        for y in 0..input_extents[1] {
            grid[[x, y, 0]] = input[[x, y]];
        }
    }

    for _ in 0..6 {
        grid = cycle_part1(&grid);
    }

    count_part1(&grid)
}

fn cycle_part1(grid: &Array3D<bool>) -> Array3D<bool> {
    let old_extents = grid.extents();
    let new_extents = [old_extents[0] + 2, old_extents[1] + 2, old_extents[2] + 2];
    let mut new_grid = Array3D::new(new_extents, false);

    for new_x in 0..new_extents[0] {
        let old_x = new_x as isize - 1;
        for new_y in 0..new_extents[1] {
            let old_y = new_y as isize - 1;
            for new_z in 0..new_extents[2] {
                let old_z = new_z as isize - 1;

                let mut count_neighbours = 0;
                let mut current_state = false;
                for dx in [-1, 0, 1] {
                    for dy in [-1, 0, 1] {
                        for dz in [-1, 0, 1] {
                            let (nx, ny, nz) = (old_x + dx, old_y + dy, old_z + dz);
                            if nx >= 0
                                && nx < old_extents[0] as isize
                                && ny >= 0
                                && ny < old_extents[1] as isize
                                && nz >= 0
                                && nz < old_extents[2] as isize
                            {
                                let (nx, ny, nz) = (nx as usize, ny as usize, nz as usize);
                                if dx == 0 && dy == 0 && dz == 0 {
                                    current_state = grid[[nx, ny, nz]];
                                } else {
                                    count_neighbours += if grid[[nx, ny, nz]] { 1 } else { 0 };
                                }
                            }
                        }
                    }
                }

                new_grid[[new_x, new_y, new_z]] = count_neighbours == 3 || (current_state && count_neighbours == 2);
            }
        }
    }

    new_grid
}

fn count_part1(grid: &Array3D<bool>) -> usize {
    let extents = grid.extents();
    let mut count = 0;
    for x in 0..extents[0] {
        let sub_grid = grid.eliminated_dim(0, x);
        for y in 0..extents[1] {
            let sub_sub_grid = sub_grid.eliminated_dim(0, y);
            count += sub_sub_grid.fold(0, |a, &b| a + if b { 1 } else { 0 })
        }
    }
    count
}

fn run_part2(input: &Array2D<bool>) -> usize {
    let input_extents = input.extents();

    let mut grid = Array4D::new([input_extents[0], input_extents[1], 1, 1], false);
    for x in 0..input_extents[0] {
        for y in 0..input_extents[1] {
            grid[[x, y, 0, 0]] = input[[x, y]];
        }
    }

    for _ in 0..6 {
        grid = cycle_part2(&grid);
    }

    count_part2(&grid)
}

fn cycle_part2(grid: &Array4D<bool>) -> Array4D<bool> {
    let old_extents = grid.extents();
    let new_extents = [
        old_extents[0] + 2,
        old_extents[1] + 2,
        old_extents[2] + 2,
        old_extents[3] + 2,
    ];
    let mut new_grid = Array4D::new(new_extents, false);

    for new_x in 0..new_extents[0] {
        let old_x = new_x as isize - 1;
        for new_y in 0..new_extents[1] {
            let old_y = new_y as isize - 1;
            for new_z in 0..new_extents[2] {
                let old_z = new_z as isize - 1;
                for new_w in 0..new_extents[3] {
                    let old_w = new_w as isize - 1;

                    let mut count_neighbours = 0;
                    let mut current_state = false;
                    for dx in [-1, 0, 1] {
                        for dy in [-1, 0, 1] {
                            for dz in [-1, 0, 1] {
                                for dw in [-1, 0, 1] {
                                    let (nx, ny, nz, nw) = (old_x + dx, old_y + dy, old_z + dz, old_w + dw);
                                    if nx >= 0
                                        && nx < old_extents[0] as isize
                                        && ny >= 0
                                        && ny < old_extents[1] as isize
                                        && nz >= 0
                                        && nz < old_extents[2] as isize
                                        && nw >= 0
                                        && nw < old_extents[3] as isize
                                    {
                                        let (nx, ny, nz, nw) = (nx as usize, ny as usize, nz as usize, nw as usize);
                                        if dx == 0 && dy == 0 && dz == 0 && dw == 0 {
                                            current_state = grid[[nx, ny, nz, nw]];
                                        } else {
                                            count_neighbours += if grid[[nx, ny, nz, nw]] { 1 } else { 0 };
                                        }
                                    }
                                }
                            }
                        }
                    }

                    new_grid[[new_x, new_y, new_z, new_w]] =
                        count_neighbours == 3 || (current_state && count_neighbours == 2);
                }
            }
        }
    }

    new_grid
}

fn count_part2(grid: &Array4D<bool>) -> usize {
    let extents = grid.extents();
    let mut count = 0;
    for x in 0..extents[0] {
        let sub_grid = grid.eliminated_dim(0, x);
        for y in 0..extents[1] {
            let sub_sub_grid = sub_grid.eliminated_dim(0, y);
            for z in 0..extents[2] {
                let sub_sub_sub_grid = sub_sub_grid.eliminated_dim(0, z);
                count += sub_sub_sub_grid.fold(0, |a, &b| a + if b { 1 } else { 0 })
            }
        }
    }
    count
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let input = parse_input(include_str!("input_example")).unwrap();

        assert_eq!(run_part1(&input), 112);
        assert_eq!(run_part2(&input), 848);
    }
}

use anyhow::{bail, ensure, Context, Result};
use lazy_static::lazy_static;
use regex::Regex;

fn main() -> Result<()> {
    let input = parse_input(include_str!("input"))?;

    println!("Part 1: {:?}", run_part1(&input)?);
    println!("Part 2: {:?}", run_part2(&input)?);

    Ok(())
}

fn parse_input(input: &str) -> Result<Vec<u32>> {
    ensure!(input.is_ascii(), "Input is not ASCII");

    let result: Result<Vec<_>, _> = input.lines().map(|l| parse_line(l)).collect();
    result.context("Failed to parse input")
}

fn parse_line(line: &str) -> Result<u32> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^[FB]{7}[LR]{3}$").unwrap();
    }

    if !RE.is_match(line) {
        bail!("Failed to parse line in input: '{}'", line);
    }

    let mut result = 0;
    for c in line.chars() {
        result <<= 1;
        if c == 'B' || c == 'R' {
            result += 1;
        }
    }
    Ok(result)
}

fn run_part1(data: &[u32]) -> Result<u32> {
    let max = data.iter().max().context("No max seat found")?;
    Ok(*max)
}

fn run_part2(data: &[u32]) -> Result<u32> {
    let mut sorted_data = data.to_vec();
    sorted_data.sort();
    let sorted_data = sorted_data;

    for i in 0..sorted_data.len() - 1 {
        if sorted_data[i + 1] - sorted_data[i] == 2 {
            return Ok(sorted_data[i] + 1);
        }
    }
    bail!("Seat not found");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_line() {
        assert_eq!(parse_line("FBFBBFFRLR").unwrap(), 357);
        assert_eq!(parse_line("BFFFBBFRRR").unwrap(), 567);
        assert_eq!(parse_line("FFFBBBFRRR").unwrap(), 119);
        assert_eq!(parse_line("BBFFBBFRLL").unwrap(), 820);
    }
}
